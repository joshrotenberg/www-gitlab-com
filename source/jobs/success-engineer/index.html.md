---
layout: job_page
title: "Success Engineer"
---

Success Engineers are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements.  Success Engineers are responsible for actively driving and managing the technology evaluation and validation stages of the sales process.  Success Engineers are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the the technical solution while also understanding the business challenges the customer is trying to overcome.

## Responsibilities

* Provide solution presentations, product demonstrations, and Proofs of Value that clearly exhibit the value GitLab will bring to prospects and clients.
* Understand customer requirements and set appropriate expectations.
* Subject Matter Expertise to the client throughout the sales process
* Developing tactical and strategic sales plans to win business for the sales teams
* Articulate the business and technical value proposition including GitLab’s key differentiation to prospective new customers and partners
* Create formal technical responses to prospects and clients
* Serve as liaison between the client and GitLab product team to effectively convey and prioritize customer requirements
* Become a Subject Matter Expert in competitive solution assessment and response
* Involved in and facilitate training workshops externally and internally
* Provide guidance on documentation


## Requirements
* You share GitLab’s values and work in accordance with those values
* You have great people skills and are able to engage an audience with a clear concise message on complex technical topics
* Deep knowledge of software development lifecycle and development pipeline (idea to production)
  * Able to convey the business value of continuous integration, continuous deployment,  chatOps, and cloud native with a understanding of the different applications used for these solutions.
  * Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss the value of different software development processes
  * Understand mono-repo and distributed-repo approaches
* Above average knowledge of Unix and Unix based Operating Systems
  * Installation and operation of Linux operating systems and hardware investigation / manipulation commands
  * BASH / Shell scripting including systemd and init.d startup scripts
  * Package management (RPM, etc. to add/remove/list packages)
  * Understanding of system log files / logging infrastructure
  * Strong desire to learn new things
* Experience with container systems
  * Kubernetes / Docker
  * Installation / configuration
  * Container registries
* Experience with any of the following tools / software packages:
  * Ruby on Rails Applications
  * Git, BitBucket/Stash, GitHub, Perforce, SVN
  * Jira, Jenkins
* B.Sc. in Computer Science or equivalent experience



## Hiring Process


Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* Qualified applicants receive a short questionnaire and coding exercise from our Global Recruiters
* The review process for this role can take a little longer than usual but if in doubt, check in with the Global recruiter at any point.
* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with a Success Engineer
* Candidates will then be invited to schedule a series of interviews with our VP of Engineering
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email


Additional details about our process can be found on our [hiring page](/handbook/hiring).
